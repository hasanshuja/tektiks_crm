
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Tektiks | CRM
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="assets/demo/demo.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet" />

</head>

<body >


<section class="login-block">
    <div class="container">
    <div class="row">
        <div class="col-md-4 login-sec">
        <div class="logo text-center">
       <!--  <a href="index.php" class="simple-text logo-normal ">
          <img src="assets/img/logo.png" alt="logo" style="width: 172px;">
        </a> -->
        <h1><i class="fa fa-paw"></i>Tektiks CRM</h1>
      </div>
            <form class="login-form" action="dashboard.php">
  <div class="form-group">
    <label for="exampleInputEmail1" class="text-uppercase">Username</label>
    <input type="text" class="form-control" placeholder="">
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="text-uppercase">Password</label>
    <input type="password" class="form-control" placeholder="">
  </div>
  
  
    <div class="form-check">
    <label class="form-check-labe">
         <div class="checkbox-animated box-content" >
                        <input id="checkbox_animated_3" class="subject-list2" type="checkbox">
                        <label for="checkbox_animated_3" >
                            <span class="check"></span>
                            <span class="box"></span>
                        <small>Remember Me</small>
                        </label>
         </div>
     
    </label>
    <button type="submit" class="btn btn-login float-right" id="button">Login</button>
  </div>
  <div class="forget-password text-center">
      <p><a href="forgetPassword.php">Lost your password?</a> </p>
  </div>
</form>
<div class="copy-text">Designed and Developed by <a href="http://tektiks.com">Tektiks</a></div>
        </div>
        <div class="col-md-8 banner-sec">
       
      <img class="d-block img-fluid" src="assets/img/bg.jpg" alt="First slide">
      
    </div>
  </div>     
</div>
</div>
</div>
</section>

</body>
</html>