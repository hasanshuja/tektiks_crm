<?php include('header.php');?>
<style type="text/css">
  
  #next{
        background-color: #ea982c;
}
#pending{
        background-color: #187bb4;
}
#previous{
        background-color: #69af69;
}
</style>

<div class="main-panel">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Edit Packages</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">

        <div class="row">
<div class="col-md-12">
    <div class="wizard_horizontal">
<ul class="wizard_steps">
                        <li>
                          <a class="selected" href="makeNewSale.php">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Basic Info<br>
                                              <!--<small>Step 1 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="selected" href="packageDetail.php">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Package Detail<br>
                                             <!-- <small>Step 2 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="selected" href="creaditCard.php">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Credit Card Info<br>
                                             <!-- <small>Step 3 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="disabled" href="accountInfo.php">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Account Info<br>
                                              <!--<small>Step 4 description</small>-->
                                          </span>
                          </a>
                        </li>
                      </ul>
         </div>
        </div>
        </div>


        <div class="row">
            <div class="col-md-9">

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Credit Card Detail</h4>

                </div>
                <div class="card-body">
               
                 <form>
                <div class="row">
                   <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Card*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Select Card--</option>
                         <option value="">Credit</option>
                         <option value="">Debit</option>
                    </select>
                        </div>
                      </div>

                     <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Card Type*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Select Card Type--</option>
                         <option value="">Visa</option>
                         <option value="">Master</option>
                         <option value="">Discovery</option>
                         <option value="">American Express</option>
                    </select>
                        </div>
                      </div>

                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">CC No*</label>
                          <input type="text" class="form-control"  >
                        </div>
                      </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">CVV No*</label>
                          <input type="text" class="form-control"  >
                        </div>
                      </div>
                  
                </div> <!-- row -->

                <div class="row">
                	     <div class="col-md-3">
                        <div class="form-group">
                   <label>Card Expiery </label>
                   <br>
                   <select class="inp-form text required" class="birth" id="bmonth" name="bmonth"
                   style="height: 34px; width:75px; background-size: 18px 37px;border: none;
    border-bottom: 1px solid #d2d2d2; " required="">

   								 <option value="">Month</option>
    							                                <option value="01">01</option>
    							                                <option value="02">02</option>
    							                                <option value="03">03</option>
    							                                <option value="04">04</option>
    							                                <option value="05">05</option>
    							                                <option value="06">06</option>
    							                                <option value="07">07</option>
    							                                <option value="08">08</option>
    							                                <option value="09">09</option>
    							                                <option value="10">10</option>
    							                                <option value="11">11</option>
    							                                <option value="12">12</option>
    														</select>
                      <select class="inp-form text required" id="byear" name="byear" style="height: 34px;  width:70px; background-size: 18px 37px;
                                border: none;
    border-bottom: 1px solid #d2d2d2;" required="">
                                      <option value="">Year</option>
                                      										<option value="2019">2019</option>
                                       										<option value="2018">2018</option>
                                       										<option value="2017">2017</option>
                                       										<option value="2016">2016</option>
                                       										<option value="2015">2015</option>
                                       										<option value="2014">2014</option>
                                       										<option value="2013">2013</option>
                                       										<option value="2012">2012</option>
                                       										<option value="2011">2011</option>
                                       										<option value="2010">2010</option>
                                       										<option value="2009">2009</option>
                                       										<option value="2008">2008</option>
                                       										<option value="2007">2007</option>
                                       										<option value="2006">2006</option>
                                       										<option value="2005">2005</option>
                                       										<option value="2004">2004</option>
                                       										<option value="2003">2003</option>
                                       										<option value="2002">2002</option>
                                       										<option value="2001">2001</option>
                                       										<option value="2000">2000</option>
                                       										<option value="1999">1999</option>
                                       										<option value="1998">1998</option>
                                       										<option value="1997">1997</option>
                                       										<option value="1996">1996</option>
                                       										<option value="1995">1995</option>
                                       										<option value="1994">1994</option>
                                       										<option value="1993">1993</option>
                                       										<option value="1992">1992</option>
                                       										<option value="1991">1991</option>
                                       										<option value="1990">1990</option>
                                       										<option value="1989">1989</option>
                                       										<option value="1988">1988</option>
                                       										<option value="1987">1987</option>
                                       										<option value="1986">1986</option>
                                       										<option value="1985">1985</option>
                                       										<option value="1984">1984</option>
                                       										<option value="1983">1983</option>
                                       										<option value="1982">1982</option>
                                       										<option value="1981">1981</option>
                                       										<option value="1980">1980</option>
                                       										<option value="1979">1979</option>
                                       										<option value="1978">1978</option>
                                       										<option value="1977">1977</option>
                                       										<option value="1976">1976</option>
                                       										<option value="1975">1975</option>
                                       										<option value="1974">1974</option>
                                       										<option value="1973">1973</option>
                                       										<option value="1972">1972</option>
                                       										<option value="1971">1971</option>
                                       										<option value="1970">1970</option>
                                       										<option value="1969">1969</option>
                                       										<option value="1968">1968</option>
                                       										<option value="1967">1967</option>
                                       										<option value="1966">1966</option>
                                       										<option value="1965">1965</option>
                                       										<option value="1964">1964</option>
                                       										<option value="1963">1963</option>
                                       										<option value="1962">1962</option>
                                       										<option value="1961">1961</option>
                                       										<option value="1960">1960</option>
                                       										<option value="1959">1959</option>
                                       										<option value="1958">1958</option>
                                       										<option value="1957">1957</option>
                                       										<option value="1956">1956</option>
                                       										<option value="1955">1955</option>
                                       										<option value="1954">1954</option>
                                       										<option value="1953">1953</option>
                                       										<option value="1952">1952</option>
                                       										<option value="1951">1951</option>
                                       										<option value="1950">1950</option>
                                       										<option value="1949">1949</option>
                                       										<option value="1948">1948</option>
                                       										<option value="1947">1947</option>
                                       										<option value="1946">1946</option>
                                       										<option value="1945">1945</option>
                                       										<option value="1944">1944</option>
                                       										<option value="1943">1943</option>
                                       										<option value="1942">1942</option>
                                       										<option value="1941">1941</option>
                                       										<option value="1940">1940</option>
                                       										<option value="1939">1939</option>
                                       										<option value="1938">1938</option>
                                       										<option value="1937">1937</option>
                                       										<option value="1936">1936</option>
                                       										<option value="1935">1935</option>
                                       										<option value="1934">1934</option>
                                       										<option value="1933">1933</option>
                                       										<option value="1932">1932</option>
                                       										<option value="1931">1931</option>
                                       										<option value="1930">1930</option>
                                       										<option value="1929">1929</option>
                                       										<option value="1928">1928</option>
                                       										<option value="1927">1927</option>
                                       										<option value="1926">1926</option>
                                       										<option value="1925">1925</option>
                                       										<option value="1924">1924</option>
                                       										<option value="1923">1923</option>
                                       										<option value="1922">1922</option>
                                       										<option value="1921">1921</option>
                                       										<option value="1920">1920</option>
                                       										<option value="1919">1919</option>
                                                                           </select>




                        </div>
                      </div><!--  col-md-3 -->
               
                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name On Card*</label>
                          <input type="text" class="form-control"  >
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Card Billing Address*</label>
                          <input type="text" class="form-control"  >
                        </div>
                     </div>

               </div><!-- row -->

               <div class="row">
               	<div class="col-md-3 col-sm-12 col-xs-12 form-group">
<div class="form-group">
<label>Required Card Detail?</label>

<div class="billing-cover" >
<div class="row">
                 <div class="col-md-6">
                 <div class="checkbox-animated box-content" >
                        <input id="checkbox_animated_5" class="subject-list3" type="checkbox">
                        <label for="checkbox_animated_5" >
                            <span class="check"></span>
                            <span class="box"></span>
                        Yes
                        </label>
                    </div>
                 </div>
                 <div class="col-md-6">
                 <div class="checkbox-animated box-content">
                        <input id="checkbox_animated_6" class="subject-list3" type="checkbox">
                        <label for="checkbox_animated_6">
                            <span class="check"></span>
                            <span class="box"></span>
                        No
                        </label>
                    </div>
                 </div>
                </div>
                </div>

</div>
</div>
               </div><!--  row -->
               </form>

                </div> <!-- card-body -->
              </div><!-- card -->
 <div>
   <a href="packagedetail.php" class="btn" id="previous" >Previous</a><a href="" class="btn" id="pending">Pending</a><a href="accountinfo.php" class="btn" id="next">Save & Continue</a>
</div>
            </div><!--  col-md-9 -->
  <div class="col-md-3">
            <div class="card ">
              <div style="padding: 10px;">
              <div class="top-cover">
              <div class="cover-pack">
              <label>Customer Name:</label>
              </div>
              <p>Usman</p>
              </div>
              
               <div class="top-cover">
              <div class="cover-pack">
              <label>Phone No:</label>
               </div>
              <p>+9211111111</p>
             </div>

               <div class="top-cover">
              <div class="cover-pack">
              <label>Email:</label>
              </div>
              <p>Test@demo.com</p>
              </div>

                <div class="top-cover">
              <div class="cover-pack">
              <label>Address</label>
              </div>
              <p>Lahore, Pakistan Lahore IA</p>
            </div>

  <div class="top-cover">
              <div class="cover-pack">
              <label>Zip-Code</label>
              </div>
              <p>54000</p>
</div>

<div class="top-cover">
              <div class="cover-pack">
              <label>Remarks</label>
               </div>
              <textarea  class="remarks-text" rows="4"></textarea>
           
            </div>
              <button class="btn " id="data-check">Data Check Status</button>

              </div>
            </div><!--  card -->
        </div><!--  col-md-3 -->
        </div> <!-- row -->
 <footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Designed and Developed by
            <a href="https://www.Tektiks.com" target="_blank">Tektiks</a> 
          </div>
        </div>
      </footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
 <script type="text/javascript">

 $('.subject-list3').on('change', function() {
		    $('.subject-list3').not(this).prop('checked', false);
		});
   
</script>

<?php include('footer.php');?>