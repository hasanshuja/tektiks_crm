<?php include('header.php');?>
<style type="text/css">
  
  #next{
        background-color: #ea982c;
}
#pending{
        background-color: #187bb4;
}
#previous{
        background-color: #69af69;
}
</style>
<div class="main-panel">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Edit Packages</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">

        <div class="row">
<div class="col-md-12">
    <div class="wizard_horizontal">
<ul class="wizard_steps">
                        <li>
                          <a class="selected" href="makeNewSale.php">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                             Basic Info<br>
                            <!--<small>Step 1 description</small>-->
                            </span>
                          </a>
                        </li>
                        <li>
                          <a  href="packageDetail.php" id="step_two" class="selected">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Package Detail<br>
                                             <!-- <small>Step 2 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="disabled" href="creaditCard.php">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Credit Card Info<br>
                                             <!-- <small>Step 3 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="disabled" href="accountInfo.php">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Account Info<br>
                                              <!--<small>Step 4 description</small>-->
                                          </span>
                          </a>
                        </li>
                      </ul>
         </div>
        </div>
        </div>


        <div class="row">
            <div class="col-md-9">

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add Package Details</h4>

                </div>
                <div class="card-body">
                <div class="add">
                <button class="btn " id="add-more">+</button>
                </div>  <!-- add -->
              <div id="field"  >
                <div id="detail">
              <form>
                <div class="row">
                   <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Provider*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Please Select--</option>
                         <option value=""></option>
                    </select>
                        </div>
                      </div>

                     <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Service*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">Select Services</option>
                         <option value=""></option>
                    </select>
                        </div>
                      </div>

                    <div class="col-md-3">
                      <div class="form-group">
                      <label class="bmd-label-floating">Liscence Expiry</label>
                     <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input w-auto" data-target="#datetimepicker4">
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
                </div>
                </div>

                  <div class="col-md-3 col-sm-12 col-xs-12 form-group btn-align text-right">
                   <input style="background-color:#00CC00; margin-top:0px;" type="submit" name="add" class="btn btn-info" value="Add">
                   </div>

                </div> <!-- row -->
               
              </form>
            </div><!-- detail -->

</div><!-- details -->
<div class="add">
                <button class="btn add_field_button" id="add-more2">+</button>
    </div>  <!-- add -->


<div class="input_fields_wrap">
   
</div>
<div class="add">
                <button class="btn add_field_button" id="add-more-three">+</button>
                </div>  <!-- add -->
 <div id="detailthree">
              <form>
                <div class="row">
                   <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Provider*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Please Select--</option>
                         <option value=""></option>
                    </select>
                        </div>
                      </div>

                     <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Select Service*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">Select Services</option>
                         <option value=""></option>
                    </select>
                        </div>
                      </div>

                    <div class="col-md-3">
                      <div class="form-group">
                      <label class="bmd-label-floating">Liscence Expiry</label>
                     <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input w-auto" data-target="#datetimepicker4">
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
                </div>
                </div>

                  <div class="col-md-3 col-sm-12 col-xs-12 form-group btn-align text-right">
                   <input style="background-color:#00CC00; margin-top:0px;" type="submit" name="add" class="btn btn-info" value="Add">
                   </div>

                </div> <!-- row -->
               
              </form>
            </div><!-- detail -->

                </div> <!-- card-body -->
              </div><!-- card -->
              <div>
   <a href="makenewsale.php" class="btn" id="previous" >Previous</a><button class="btn" id="pending">Pending</button><a href="creaditcard.php" class="btn" id="next">Next</a>
</div>
            </div><!--  col-md-9 -->
          
        <div class="col-md-3">
            <div class="card ">
              <div style="padding: 10px;">
              <div class="top-cover">
              <div class="cover-pack">
              <label>Customer Name:</label>
              </div>
              <p>Usman</p>
              </div>
              
               <div class="top-cover">
              <div class="cover-pack">
              <label>Phone No:</label>
               </div>
              <p>+9211111111</p>
             </div>

               <div class="top-cover">
              <div class="cover-pack">
              <label>Email:</label>
              </div>
              <p>Test@demo.com</p>
              </div>

                <div class="top-cover">
              <div class="cover-pack">
              <label>Address</label>
              </div>
              <p>Lahore, Pakistan Lahore IA</p>
            </div>

  <div class="top-cover">
              <div class="cover-pack">
              <label>Zip-Code</label>
              </div>
              <p>54000</p>
</div>

<div class="top-cover">
              <div class="cover-pack">
              <label>Remarks</label>
               </div>
              <textarea  class="remarks-text" rows="4"></textarea>
           
            </div>
              <button class="btn " id="data-check">Data Check Status</button>

              </div>
            </div><!--  card -->
        </div><!--  col-md-3 -->

          </div><!--  row -->
</div>
</div>
<footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Designed and Developed by
            <a href="https://www.Tektiks.com" target="_blank">Tektiks</a> 
          </div>
        </div>
      </footer>
</div>
 <!-- main-panel -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script> -->

<!-- <script type="text/javascript">

  $(document).ready(function(){
    $('#field').hide();
      $('#add-more2').hide();
     $('#detailthree').hide();
     $('#add-more-three').hide();
    $('#add-more').on('click',function(){
        $('#field').slideToggle(function(){
           $('#add-more2').show();
        });
    });

     $('#add-more2').on('click',function(){
        $('#details').slideToggle(function(){
            $('#add-more-three').show();
        });
       
    });

$('#add-more-three').on('click',function(){
         
         $('#detailthree').slideToggle();
        // alert('ok');
       
    });






$(document).ready(function() {
  var max_fields      = 99; //maximum input boxes allowed
  var wrapper       = $(".input_fields_wrap"); //Fields wrapper
  var wrapper2       = $(".input_fields_wrap2"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  
  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment
      $(wrapper).append('<div><div id="details" style=" "><form><div class="row"><div class="col-md-3"><div class="form-group"><label class="bmd-label-floating">Select Provider*</label><select name="unit_type" class="form-control" id="unit_type" required=""><option value="">--Please Select--</option><option value=""></option></select></div></div><div class="col-md-3"><div class="form-group"><label class="bmd-label-floating">Select Service*</label><select name="unit_type" class="form-control" id="unit_type" required=""><option value="">Select Services</option><option value=""></option></select></div></div><div class="col-md-3"><div class="form-group"><label class="bmd-label-floating">Liscence Expiry</label><div class="input-group date" id="datetimepicker4" data-target-input="nearest"><input type="text" class="form-control datetimepicker-input w-auto" data-target="#datetimepicker4"><div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker"><div class="input-group-text"><i class="fa fa-calendar"></i></div></div></div></div></div><div class="col-md-3 col-sm-12 col-xs-12 form-group btn-align text-right"><input style="background-color:#00CC00; margin-top:0px;" type="submit" name="add" class="btn btn-info" value="Add"></div></div></form>  add </div></div>');add input box

   

  });
  
 
});

  }); -->


  


<!-- </script> --> 

<?php include('footer.php');?>