<?php include('header.php');?>
<style type="text/css">
  
  #next{
        background-color: #ea982c;
}
#pending{
        background-color: #187bb4;
}
#previous{
        background-color: #69af69;
}
</style>

<div class="main-panel">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Edit Packages</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">

        <div class="row">
<div class="col-md-12">
    <div class="wizard_horizontal">
<ul class="wizard_steps">
                        <li>
                          <a class="selected" href="makeNewSale.php">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Basic Info<br>
                                              <!--<small>Step 1 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="selected" h  ref="packageDetail.php">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Package Detail<br>
                                             <!-- <small>Step 2 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="selected" href="creaditCard.php">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Credit Card Info<br>
                                             <!-- <small>Step 3 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="selected" href="accountInfo.php">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Account Info<br>
                                              <!--<small>Step 4 description</small>-->
                                          </span>
                          </a>
                        </li>
                      </ul>
         </div>
        </div>
        </div>


        <div class="row">
            <div class="col-md-9">

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Huge Net</h4>

                </div>
                <div class="card-body">
               
                 <form>
                <div class="row">
                   <div class="col-md-3">
                        
                         <div class="form-group">
                          <label class="bmd-label-floating">Account No*</label>
                          <input type="text" class="form-control"  >
                        </div>
                        
                      </div>

                      <div class="col-md-3">
                        
                         <div class="form-group">
                          <label class="bmd-label-floating">OCN No*</label>
                          <input type="text" class="form-control"  >
                        </div>
                        
                      </div>

                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">REF No*</label>
                          <input type="text" class="form-control"  >
                        </div>
                      </div>
                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">New Number*</label>
                          <input type="text" class="form-control"  >
                        </div>
                      </div>
                  
                </div> <!-- row -->

                <div class="row">
                	    
                       <div class="col-md-3">
                      <div class="form-group">
                      <label class="bmd-label-floating">Appt Date</label>
                     <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input w-auto" data-target="#datetimepicker4">
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                </div>
                      </div>
               
                     <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Appt Time*</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Select Card--</option>
                         <option value="">Credit</option>
                         <option value="">Debit</option>
                    </select>
                        </div>
                      </div>

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Monthly Bill*</label>
                          <input type="text" class="form-control"  >
                        </div>
                     </div>

                       <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Screen Shot*</label>
                          <input type="file" class="form-control">
                        </div>
                     </div>

               </div><!-- row -->
               
                
                <div class="row pull-right" >
                  <div class="col-md-4">
                    <div class="update-btn">
                 <button class="btn ">Update</button>
               </div>
                  </div>
                </div>

               </form>
              </div> <!-- card-body -->
              </div><!-- card -->
 <div>
   <a href="creaditcard.php" class="btn" id="previous" >Previous</a><a  href="" class="btn" id="pending">Pending</a><a  href="" class="btn" id="next">End</a>
</div>
            </div><!--  col-md-9 -->
  <div class="col-md-3">
            <div class="card ">
              <div style="padding: 10px;">
              <div class="top-cover">
              <div class="cover-pack">
              <label>Customer Name:</label>
              </div>
              <p>Usman</p>
              </div>
              
               <div class="top-cover">
              <div class="cover-pack">
              <label>Phone No:</label>
               </div>
              <p>+9211111111</p>
             </div>

               <div class="top-cover">
              <div class="cover-pack">
              <label>Email:</label>
              </div>
              <p>Test@demo.com</p>
              </div>

                <div class="top-cover">
              <div class="cover-pack">
              <label>Address</label>
              </div>
              <p>Lahore, Pakistan Lahore IA</p>
            </div>

  <div class="top-cover">
              <div class="cover-pack">
              <label>Zip-Code</label>
              </div>
              <p>54000</p>
</div>

<div class="top-cover">
              <div class="cover-pack">
              <label>Remarks</label>
               </div>
              <textarea  class="remarks-text" rows="4"></textarea>
           
            </div>
              <button class="btn " id="data-check">Data Check Status</button>

              </div>
            </div><!--  card -->
        </div><!--  col-md-3 -->
        </div> <!-- row -->
 <footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Designed and Developed by
            <a href="https://www.Tektiks.com" target="_blank">Tektiks</a> 
          </div>
        </div>
      </footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
 <script type="text/javascript" src="assets/js/custom.js"></script>

<script type="text/javascript">
  
<?php include('footer.php');?>