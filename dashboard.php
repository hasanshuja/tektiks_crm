<?php include('header.php');?>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="card-header card-header-primary mb-4 bg-primary col-md-12">
              <div class="row">
                <h3 class="card-title text-white mb-0 col-md-7 my-auto"><b>Today's Target</b></h3>
                <div class="w-100 col-md-5 text-right my-auto">
                <h4 class="float-left text-white mt-3 mr-3"><b>Sort By Date:</b></h4>
                <form>
                <input type="date" name="" class="form-control text-white col-md-8">
                </form>
              </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">S</i>
                  </div>
                  <h3 class="card-title h4"><b>Spectrum</b>
                  </h3>
                  <h3 class="text-dark">0</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">D</i>
                  </div>
                  <h3 class="card-title h4"><b>DTV</b></h3>
                  <h3 class="text-dark">0</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">A</i>
                  </div>
                  <h3 class="card-title h4"><b>AT&T</b></h3>
                  <h3 class="text-dark">0</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">E</i>
                  </div>
                  <h3 class="card-title h4"><b>EXCEED</b></h3>
                  <h3 class="text-dark">0</h3>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="card-header card-header-primary mb-4 bg-primary col-md-12">
              <div class="row">
                <h3 class="card-title text-white mb-0 col-md-7 my-auto"><b>Target Achived</b></h3>
                <div class="w-100 col-md-5 text-right my-auto">
                <h4 class="text-white mt-3 mr-3"><b>11-18-2019 Scheduled RGU'S</b></h4>
              </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart h4" id="">Total SALES</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart h4" id="">Total RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 5 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">SPECTRUM RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 2 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart h4" id="">DTV RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 1 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">Uverse RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart h4" id="">AT&T (INT) RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 1 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart h4" id="">AT&T(PH) RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">MOBILITY RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart h4" id="">EXEDE RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">Century RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 1 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart h4" id="">Cincinati RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 1 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">Frontier RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 1 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">Hugenet RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart h4" id="">Windstream RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart h4" id="">SECURITY RGU'S</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="card card-chart">
                <div class="card-header card-header-danger">
                  <div class="ct-chart h4" id="">DTVNOW</div>
                </div>
                <div class="card-body">
                  <p class="card-category">
                    <span class="text-success h3"><i class="fa fa-long-arrow-up"></i> 0 </span></p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title text-white mb-0 col-md-7 my-auto"><b>Reports</b></h3>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <tr><th>
                        Name
                        </th>
                        <th>
                          Active
                        </th>
                        <th>
                          Schedule
                        </th>
                        <th>
                          Cancel
                        </th>
                        <th>
                          Inprocess
                        </th>
                        <th>Pending</th>
                        <th>Reject</th>
                        <th>Disconnect</th>
                        <th>Action</th>
                      </tr></thead>
                      <tbody>
                        <tr>
                          <td>
                            Spectrum
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            DTV
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Uverse
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            AT& T(Int)
                          </td>
                          <td>
                          0
                          </td>
                          <td>
                         0
                          </td>
                          <td>
                          0
                          </td>
                          <td>
                         0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            AT& T(Phone)
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Mobility
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Exceed
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Century
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Cincinati
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Frontier
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Hugenet
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Windstream
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                          <td>
                            Security
                          </td>
                          <td>
                           0
                          </td>
                          <td>
                        0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                          <td>
                            0
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
      </div>


 <?php include('footer.php');?>
