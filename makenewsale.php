<?php include('header.php');?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>

    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Sale Form</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">

        <div class="row">
<div class="col-md-12">
    <div class="wizard_horizontal">
<ul class="wizard_steps">
                        <li>
                          <a class="selected" href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Basic Info<br>
                                              <!--<small>Step 1 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a  class="disabled"  href="packageDetail.php" id="step_two">
                            <span  class=" step_no" >2</span>
                            <span class="step_descr">
                                              Package Detail<br>
                                             <!-- <small>Step 2 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="disabled"  href="creaditCard.php">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Credit Card Info<br>
                                             <!-- <small>Step 3 description</small>-->
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a class="disabled"  href="accountInfo.php">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              Account Info<br>
                                              <!--<small>Step 4 description</small>-->
                                          </span>
                          </a>
                        </li>
                      </ul>
</div>
        </div>
        </div>

          <div class="row">
            <div class="col-md-12">

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Customer Information</h4>

                </div>
                <div class="card-body">
                  <form>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">First Name</label>
                          <input type="text" class="form-control" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Middle Name</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" class="form-control" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone No</label>
                          <input type="text" class="form-control" >
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Alt Phone No</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>


                    <div class="col-md-3">
                        <div class="form-group">
                   <label>Date Of Birth *:</label>
                   <br>
                   <select class="inp-form text required" class="birth" id="bmonth" name="bmonth"
                   style="height: 34px; width:55px; background-size: 18px 37px;border: none;
    border-bottom: 1px solid #d2d2d2; " required="">

   								 <option value="">Month</option>
    							                                <option value="01">01</option>
    							                                <option value="02">02</option>
    							                                <option value="03">03</option>
    							                                <option value="04">04</option>
    							                                <option value="05">05</option>
    							                                <option value="06">06</option>
    							                                <option value="07">07</option>
    							                                <option value="08">08</option>
    							                                <option value="09">09</option>
    							                                <option value="10">10</option>
    							                                <option value="11">11</option>
    							                                <option value="12">12</option>
    														</select>
<select class="inp-form text required" id="bday" name="bday" style="height: 34px;  width:55px; background-size: 18px 37px;
border: none;border-bottom: 1px solid #d2d2d2;"
 required="" >
                                  <option value="">Day</option>
                                  <option value="01">01</option>
    							                                <option value="02">02</option>
    							                                <option value="03">03</option>
    							                                <option value="04">04</option>
    							                                <option value="05">05</option>
    							                                <option value="06">06</option>
    							                                <option value="07">07</option>
    							                                <option value="08">08</option>
    							                                <option value="09">09</option>
    							                                <option value="10">10</option>
    							                                <option value="11">11</option>
    							                                <option value="12">12</option>
                                </select>

                                <select class="inp-form text required" id="byear" name="byear" style="height: 34px;  width:55px; background-size: 18px 37px;
                                border: none;
    border-bottom: 1px solid #d2d2d2;" required="">
                                      <option value="">Year</option>
                                      										<option value="2019">2019</option>
                                       										<option value="2018">2018</option>
                                       										<option value="2017">2017</option>
                                       										<option value="2016">2016</option>
                                       										<option value="2015">2015</option>
                                       										<option value="2014">2014</option>
                                       										<option value="2013">2013</option>
                                       										<option value="2012">2012</option>
                                       										<option value="2011">2011</option>
                                       										<option value="2010">2010</option>
                                       										<option value="2009">2009</option>
                                       										<option value="2008">2008</option>
                                       										<option value="2007">2007</option>
                                       										<option value="2006">2006</option>
                                       										<option value="2005">2005</option>
                                       										<option value="2004">2004</option>
                                       										<option value="2003">2003</option>
                                       										<option value="2002">2002</option>
                                       										<option value="2001">2001</option>
                                       										<option value="2000">2000</option>
                                       										<option value="1999">1999</option>
                                       										<option value="1998">1998</option>
                                       										<option value="1997">1997</option>
                                       										<option value="1996">1996</option>
                                       										<option value="1995">1995</option>
                                       										<option value="1994">1994</option>
                                       										<option value="1993">1993</option>
                                       										<option value="1992">1992</option>
                                       										<option value="1991">1991</option>
                                       										<option value="1990">1990</option>
                                       										<option value="1989">1989</option>
                                       										<option value="1988">1988</option>
                                       										<option value="1987">1987</option>
                                       										<option value="1986">1986</option>
                                       										<option value="1985">1985</option>
                                       										<option value="1984">1984</option>
                                       										<option value="1983">1983</option>
                                       										<option value="1982">1982</option>
                                       										<option value="1981">1981</option>
                                       										<option value="1980">1980</option>
                                       										<option value="1979">1979</option>
                                       										<option value="1978">1978</option>
                                       										<option value="1977">1977</option>
                                       										<option value="1976">1976</option>
                                       										<option value="1975">1975</option>
                                       										<option value="1974">1974</option>
                                       										<option value="1973">1973</option>
                                       										<option value="1972">1972</option>
                                       										<option value="1971">1971</option>
                                       										<option value="1970">1970</option>
                                       										<option value="1969">1969</option>
                                       										<option value="1968">1968</option>
                                       										<option value="1967">1967</option>
                                       										<option value="1966">1966</option>
                                       										<option value="1965">1965</option>
                                       										<option value="1964">1964</option>
                                       										<option value="1963">1963</option>
                                       										<option value="1962">1962</option>
                                       										<option value="1961">1961</option>
                                       										<option value="1960">1960</option>
                                       										<option value="1959">1959</option>
                                       										<option value="1958">1958</option>
                                       										<option value="1957">1957</option>
                                       										<option value="1956">1956</option>
                                       										<option value="1955">1955</option>
                                       										<option value="1954">1954</option>
                                       										<option value="1953">1953</option>
                                       										<option value="1952">1952</option>
                                       										<option value="1951">1951</option>
                                       										<option value="1950">1950</option>
                                       										<option value="1949">1949</option>
                                       										<option value="1948">1948</option>
                                       										<option value="1947">1947</option>
                                       										<option value="1946">1946</option>
                                       										<option value="1945">1945</option>
                                       										<option value="1944">1944</option>
                                       										<option value="1943">1943</option>
                                       										<option value="1942">1942</option>
                                       										<option value="1941">1941</option>
                                       										<option value="1940">1940</option>
                                       										<option value="1939">1939</option>
                                       										<option value="1938">1938</option>
                                       										<option value="1937">1937</option>
                                       										<option value="1936">1936</option>
                                       										<option value="1935">1935</option>
                                       										<option value="1934">1934</option>
                                       										<option value="1933">1933</option>
                                       										<option value="1932">1932</option>
                                       										<option value="1931">1931</option>
                                       										<option value="1930">1930</option>
                                       										<option value="1929">1929</option>
                                       										<option value="1928">1928</option>
                                       										<option value="1927">1927</option>
                                       										<option value="1926">1926</option>
                                       										<option value="1925">1925</option>
                                       										<option value="1924">1924</option>
                                       										<option value="1923">1923</option>
                                       										<option value="1922">1922</option>
                                       										<option value="1921">1921</option>
                                       										<option value="1920">1920</option>
                                       										<option value="1919">1919</option>
                                                                           </select>




                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="email" class="form-control">
                        </div>
                      </div>

                    <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Street No</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Address</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">City</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Zip Code</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">House Type</label>
                          <select name="house_type" class="form-control" id="house_type" required="">
                       <option value="" selected="">--Select House Type--</option>
                       <option value="house">House</option>
                       <option value="apartment">Apartment</option>
                     </select>
                        </div>
                      </div>


                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Owner Ship</label>
                          <select name="ownership_status" class="form-control" required="">
                       <option value="">--Select Owner Ship--</option>
                       <option value="Rental">Rental</option>
                       <option value="Owner">Owner</option>
                     </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Unit Type</label>
                          <select name="unit_type" class="form-control" id="unit_type" required="">
                          <option value="">--Select Unit Type--</option>
                         <option value=""></option>
                    </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Unit Number</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                    </div>



                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Pre Account No</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>


                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">SSN</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Driving Liscence</label>
                          <input type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                      <div class="form-group">
                      <label class="bmd-label-floating">Liscence Expiry</label>
                     <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                    <input type="text" class="form-control datetimepicker-input w-auto" data-target="#datetimepicker4">
                    <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                </div>
                      </div>
                    </div>

                    <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Driving State</label>
                        <select id="drvState" name="driving_state" class="form-control">
                                    <option value="" selected="">--Select Driving State--</option>                                        <option value="AK">Alaska-AK</option>

                                        <option value="HI">Hawaii-HI</option>

                                        <option value="CA">California-CA</option>

                                        <option value="NV">Nevada-NV</option>

                                        <option value="OR">Oregon-OR</option>

                                        <option value="WA">Washington-WA</option>

                                        <option value="AZ">Arizona-AZ</option>

                                        <option value="CO">Colorada-CO</option>

                                        <option value="ID">Idaho-ID</option>

                                        <option value="MT">Montana-MT</option>

                                        <option value="NE">Nebraska-NE</option>

                                        <option value="NM">New Mexico-NM</option>

                                        <option value="ND">North Dakota-ND</option>

                                        <option value="UT">Utah-UT</option>

                                        <option value="WY">Wyoming-WY</option>

                                        <option value="AL">Alabama-AL</option>

                                        <option value="AR">Arkansas-AR</option>

                                        <option value="IL">Illinois-IL</option>

                                        <option value="IA">Iowa-IA</option>

                                        <option value="KS">Kansas-KS</option>

                                        <option value="KY">Kentucky-KY</option>

                                        <option value="LA">Louisiana-LA</option>

                                        <option value="MN">Minnesota-MN</option>

                                        <option value="MS">Mississippi-MS</option>

                                        <option value="MO">Missouri-MO</option>

                                        <option value="OK">Oklahoma-OK</option>

                                        <option value="SD">South Dakota-SD</option>

                                        <option value="TX">Texas-TX</option>

                                        <option value="TN">Tennessee-TN</option>

                                        <option value="WI">Wisconsin-WI</option>

                                        <option value="CT">Connecticut-CT</option>

                                        <option value="DE">Delaware-DE</option>

                                        <option value="FL">Florida-FL</option>

                                        <option value="GA">Georgia-GA</option>

                                        <option value="IN">Indiana-IN</option>

                                        <option value="ME">Maine-ME</option>

                                        <option value="MD">Maryland-MD</option>

                                        <option value="MA">Massachusetts-MA</option>

                                        <option value="MI">Michigan-MI</option>

                                        <option value="NH">New Hampshire-NH</option>

                                        <option value="NJ">New Jersey-NJ</option>

                                        <option value="NY">New York-NY</option>

                                        <option value="NC">North Carolina-NC</option>

                                        <option value="OH">Ohio-OH</option>

                                        <option value="PA">Pennsylvania-PA</option>

                                        <option value="RI">Rhode Island-RI</option>

                                        <option value="SC">South Carolina-SC</option>

                                        <option value="VT">Vermont-VT</option>

                                        <option value="VA">Virginia-VA</option>

                                        <option value="WV">West Virginia-WV</option>



                                    </select>
                        </div>
                      </div>
                    </div>




<div class="row" >
<div class="col-md-6 col-sm-12 col-xs-12 ">
<div class="form-group">
 <label>Remarks</label>
  	<textarea class="form-control" rows="2" name="remarks" id="remarks"></textarea>
</div>
</div>

<div class="col-md-3 col-sm-12 col-xs-12 form-group">
<div class="form-group">

  <!-- ANIMATED CHECKBOXES -->
  <div class="form-group">
                    <label>Auto Billing</label>

                    <div class="billing-cover" >
                  <div class="row">
                 <div class="col-md-3">
                 <div class="checkbox-animated box-content" >
                        <input id="checkbox_animated_1" class="subject-list" type="checkbox">
                        <label for="checkbox_animated_1" >
                            <span class="check"></span>
                            <span class="box"></span>
                        Yes
                        </label>
                    </div>
                 </div>
                 <div class="col-md-6">
                 <div class="checkbox-animated box-content">
                        <input id="checkbox_animated_2" class="subject-list" type="checkbox">
                        <label for="checkbox_animated_2">
                            <span class="check"></span>
                            <span class="box"></span>
                        No
                        </label>
                    </div>
                 </div>
                </div>
                </div>

</div>
</div>


</div>

<div class="col-md-3 col-sm-12 col-xs-12 form-group">
<div class="form-group">
<label>Send For Check Data?</label>

<div class="billing-cover" >
<div class="row">
                 <div class="col-md-3">
                 <div class="checkbox-animated box-content" >
                        <input id="checkbox_animated_3" class="subject-list2" type="checkbox">
                        <label for="checkbox_animated_3" >
                            <span class="check"></span>
                            <span class="box"></span>
                        Yes
                        </label>
                    </div>
                 </div>
                 <div class="col-md-6">
                 <div class="checkbox-animated box-content">
                        <input id="checkbox_animated_4" class="subject-list2" type="checkbox">
                        <label for="checkbox_animated_4">
                            <span class="check"></span>
                            <span class="box"></span>
                        No
                        </label>
                    </div>
                 </div>
                </div>
                </div>

</div>
</div>


</div>
 
<a href="packagedetail.php" id="save" class="btn btn-primary pull-right" style=" background-color: #0f75bc;">Save and Continue</a> 
<button type="submit" class="btn btn-info pull-right" style=" background-color :#90c845; " >Save as Lead</button>

                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
         
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Designed and Developed by
            <a href="https://www.Tektiks.com" target="_blank">Tektiks</a> 
          </div>
        </div>
      </footer>
    </div>
  </div>

  <!--   Core JS Files   -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script> 

  <script>
    $(document).ready(function() {

$('#save').on('click',function(){
      $('#step_two').addClass('selected');

});


      $().ready(function() {
        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });

       jQuery(function($) {
         var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
         $('.sidebar .nav li a').each(function() {
          if (this.href === path) {
           $(this).addClass('active');
          }
          else{
            $(this).removeClass('active');
          }
         });
        });



    });



  </script>

<script type="text/javascript" src="assets/js/custom.js"></script>

</body>

</html>
